#!/bin/bash
# v1.0

DIRECTORY="$(cd "$(dirname "$0")" && pwd)"
cd "$DIRECTORY"

set -e

source ../env.sh
docker service ps --no-trunc "$SERVICE"_"$SERVICE"

for f in $(docker service ps -q "$SERVICE"_"$SERVICE");do docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f; done
echo $TEMP
