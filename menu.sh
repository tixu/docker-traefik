#!/bin/bash
# v1.0

DIRECTORY="$(cd "$(dirname "$0")" && pwd)"
cd "$DIRECTORY" || exit

source ./env.sh

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Build docker and deploy docker-compose   "
TITLE="Build and deploy : : $SERVICE"
MENU="Choose one of the following options:"

if [ -d ./build ] 
then
OPTIONS=(1 "Build"
         2 "Undeploy"
         3 "Deploy"
         4 "Status service")
else
OPTIONS=(
         2 "Undeploy"
         3 "Deploy")
fi

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear

case $CHOICE in
        1)  ./build/build.sh
            ;;
        2)  ./tools/undeploy.sh
            ;;
        3)  ./tools/deploy.sh
            ;;
	4)  ./tools/status.sh
	    ;;
esac
