#!/bin/bash

echo -e "\\033[4;34m env.sh Values :\\033[0m"

function exportPrint {
    export "$1"="$2"
    OUTPUT="$OUTPUT $1|$2\\n"
}

OUTPUT=""

shopt -s expand_aliases
alias dockerRun="docker --tlsverify"

exportPrint DOCKER_HOST "tcp://morpheus.alin.be:2376"
exportPrint DOCKER_CERT_PATH "/Users/pa/.docker/"
exportPrint SERVICE "traefik"
exportPrint REGISTRY "morpheus.alin.be:5000"
exportPrint POSTFIX "alin"
exportPrint IMAGE_TRAEFIK "traefik"
exportPrint TAG_TRAEFIK "v1.7.10-alpine"
#1.7.10-alpine"
exportPrint DOCKER_TLS_VERIFY 1

echo -e "$OUTPUT" | column -t -s'|'
